console.log(`Bot starting...`);

const Discord = require("discord.js");
const google = require('google');

function sleep(millis) {
  return new Promise(resolve => setTimeout(resolve, millis));
}
const client = new Discord.Client();

const config = require("./config.json");
const token = require("./token.json");

google.resultsPerPage = config.resultsPerPage;

client.on("ready", () => {
  console.log(`Bot has started, with ${client.users.size} users, in ${client.channels.size} channels of ${client.guilds.size} guilds.`);
  client.user.setActivity(`Being Bry`);
});

client.on("guildCreate", guild => {
  console.log(`New guild joined: ${guild.name} (id: ${guild.id}). This guild has ${guild.memberCount} members!`);
  client.user.setActivity(`Serving ${client.guilds.size} servers`);
  client.user.setGame(`on ${client.guilds.size} servers`);
});

client.on("guildDelete", guild => {
  console.log(`I have been removed from: ${guild.name} (id: ${guild.id})`);
  client.user.setActivity(`Serving ${client.guilds.size} servers`);
  client.user.setGame(`on ${client.guilds.size} servers`);
});

var i, j;
var args, command;
var arr, word, time, res;
var id, line, lines, m;

client.on("message", async message => {
	
  for (i = 0; i < config.reacts.length; i++) {
    for (j = 1; j < config.reacts[i].length; j++) {
      if (message.content.toLowerCase().indexOf(config.reacts[i][j]) !== -1) {
				message.react(config.reacts[i][0]);
				console.log(i + ", " + j);
      }
    }
  }

  if (message.author.bot) return;
  
  if (config.blacklist.indexOf(message.author.id) !== -1) return;

  // command = say
  // args = ["no", "u", "asdf"]
  
  if (message.channel.name.includes(config.nochannel)) return;
  
  args = message.content.slice(config.prefix.length).trim().split(/ +/g);
  
  command = args.shift().toLowerCase().replace(/[^a-z0-9]/g, '');
   
  for (i = 0; i < config.censors.length; i++) {
    if (message.content.indexOf(config.censors[i]) != -1) {
      message.channel.send("Bry does not condone this foul language on his good Bryrish server.");
    }
  }

  if (message.content.indexOf(config.prefix) !== 0) return;
	
	for (i = 0; i < config.asciis.length; i++) {
    arr = config.asciis[i];
    word = arr[0];
    time = arr[1];
    if (command === word) {
      if (time > -1) {
				word = args.join(" ");
				res = await message.channel.send(arr[2].replace("BRYBOTSEP", word));
				await sleep(time);
				for (j = 3; j < arr.length; j++) {
	  			res.edit(arr[j].replace("BRYBOTSEP", word));
	  			await sleep(time);
				}
				break;
      } else {
				res = args.join(" ");
				message.channel.send(arr[2].replace("BRYBOTSEP", res));
      }
    }
  }

	if (command === "ship") {
    i = Math.random();
   	i = config.starts.length * i;
    i = i - i % 1;
		j = Math.random();
		j = config.ends.length * j;
		j = j - j % 1;
		while (j === i) {
			j = Math.random();
			j = config.ends.length * j;
			j = j - j % 1;
		}
		message.channel.send("Bry declares that " + config.starts[i] + config.ends[j] + " is the new hip ship in town.");
	}

  if (command === "calculate") {
    if (args.join(" ").indexOf("token") !== -1 || args.join(" ").indexOf("console") !== -1 || (config.admins.indexOf(message.author.id) === -1 && args.join(" ").match(/[a-z]/i))) { // No, Johnny.
      message.channel.send("Bry cannot comprehend your primitive speech pattern.");
    } else {
      try {
				console.log(message.author + " sent: " + args.join(" "));
				res = eval(args.join(" "));
				message.channel.send("Bry has blessed you with his holy knowledge, and proclaims that your answer is " + res+ ".");
      } catch (e) {
        message.channel.send("Bry cannot comprehend your primitive speech pattern.");
      }
    }
  }

  if (command === "bday") {
    res = args.join(" ");
    message.delete().catch(O_o=>{});
    message.channel.send(message.author + " would like to wish " + res + " a happy bird day." + "\nhttp://itsyourbirthday.today/#" + res);
  }

  if (command === "tellme") {
    res = "Your lord has found the following information on your request";
    google(args.join(" "), function(err, res) {
      if (err) console.error(err);
      for (i = 0; i < google.resultsPerPage; i++) {
				if (res.links[i] !== null && res.links[i].description !== null) {
	  			message.channel.send(res.links[i].title + "\n" + res.links[i].href);
				}
      }
    });
  }
 
  if (command === "say") {
    res = args.join(" ");
    message.delete().catch(O_o=>{}); 
    message.channel.send(res);
  } 

  if (command === "pickup") {
    if (!isNaN(parseInt(args[0])) && parseInt(args[0]) < config.pickups.length) {
      id = parseInt(args[0]);
      args = args.slice(1,args.length);
    } else {
      id = Math.random();
      id = config.pickups.length * id;
      id = id - id % 1;
    }
    line = config.pickups[id];
    lines = line.split("BRYBOTSEP");
		m = "";
		if (args.length > 0) {
    	m = args[1] + ", ";
		}
		console.log(lines);
    for (i = 0; i < lines.length; i++) {
			console.log(i);
      m += lines[i];
      message.channel.send(m);
    	//await sleep(1000);
      m = "";
		}
	}
	
	if (config.ynwords.includes(command)) {    
  	if (message.content.indexOf("bloat") != -1) {
  		message.channel.send("Brybot believes that if you are asking if something is bloat, it probably is.");
  	} else if (Math.random() > 0.5) {
  		message.channel.send("no");
  	} else {
  		message.channel.send("yes");
  	}
  }
  
	if (config.qwords.includes(command)) {
    message.channel.send(config.words[parseInt(Math.random() * config.words.length)]);
  }

  if (command === "roast") {
    if (!isNaN(parseInt(args[0])) && parseInt(args[0]) < config.roasts.length) {
      id = parseInt(args[0]);
      args = args.slice(1,args.length);
    } else {
      id = Math.random();
      id = config.roasts.length * id;
      id = id - id%1;
    }
    if (args.length == 0) args.push(message.author);
    message.channel.send(args.join(" ") + config.roasts[id]);
  }
  
  if (command === "cox") {
    if (!isNaN(parseInt(args[0])) && parseInt(args[0]) < config.coxes.length) {
      id = parseInt(args[0]);
      args = args.slice(1,args.length);
    } else {
      id = Math.random();
      id = config.coxes.length * id;
      id = id - id % 1;
    }
    message.channel.send(config.coxes[id]);
  }

  if (command === "die" || command === "stop") {
    message.channel.send("no");
  }

});

client.login(token.token);
